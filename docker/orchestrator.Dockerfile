ARG FROM_IMAGE_REGISTRY=registry.gitlab.com
ARG FROM_IMAGE_TAG
ARG FROM_IMAGE_NAME
FROM $FROM_IMAGE_REGISTRY/$FROM_IMAGE_NAME:$FROM_IMAGE_TAG


COPY --chown=${USERNAME}:${USER_UID} . /workspace/dobas/

# make sure the workdir is owned by the user
USER root
RUN chown -R ${USERNAME}:${USER_UID} .
USER ${USERNAME}
