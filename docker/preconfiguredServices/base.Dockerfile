FROM ubuntu:20.04

# this image does handle the run-dependencies for development images as well as the production orchestrator one

ENV DEBIAN_FRONTEND=noninteractive

# default config
ENV BORG_BASE_DIR=/borgconfig
ENV CRON_INTERVAL_INCREMENTAL="59 2 * * *"

RUN apt-get update && apt-get install -y --no-install-recommends \
        borgbackup \
        cron \
        tini \
        curl \
        ca-certificates \
        git \
        jq \
        mariadb-backup \
        openssh-client \
        python3-pip \
        tzdata \
        sudo \
        && \
        rm -rf /var/cache/apt /var/lib/apt/lists

COPY checksums.txt .

######## install docker client ###########
ARG DOCKERVERSION=19.03.9
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
  && sha1sum -c checksums.txt \
  && tar xzvf docker-${DOCKERVERSION}.tgz --strip 1 \
                 -C /usr/local/bin docker/docker \
  && rm docker-${DOCKERVERSION}.tgz

######## install python packages ###########
ADD requirements-base.txt .
RUN pip3 install -r requirements-base.txt

# deactivate hashed known hosts to be able to correctly prepopulate the known host via env var
RUN sed -i 's/HashKnownHosts yes/HashKnownHosts no/' /etc/ssh/ssh_config

########### setup user ###############
ARG USERNAME=dobas
ENV USERNAME=${USERNAME}
ARG USER_UID=1000
ENV USER_UID=${USER_UID}
ARG USER_GID=$USER_UID
ENV USER_GID=${USER_UID}

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# add user to docker group to get access to docker client
RUN groupadd docker && \
    usermod -aG docker $USERNAME

# create an manage cron access fot the user
RUN echo "$USERNAME ALL=NOPASSWD: /usr/sbin/cron" >>/etc/sudoers

# setup volume directories and their ownerships
RUN mkdir -p /borgconfig /backups /bundles
RUN chown -R $USER_UID:$USER_GID /borgconfig /backups /bundles 

# some default variable for the env vars
ENV BORG_BASE_DIR=/borgconfig
ENV CRON_INTERVAL_INCREMENTAL="59 2 * * *"

# prepare stdout pipe for cronjobs
RUN ln -sf /proc/$$/fd/1 /var/log/output && \
    ln -sf /proc/$$/fd/2 /var/log/errors

# Set the default user
USER $USERNAME
WORKDIR /workspace/dobas/

ENTRYPOINT ["/usr/bin/tini", "-e", "143", "--"]
CMD ["/workspace/dobas/entrypoint.sh"]
