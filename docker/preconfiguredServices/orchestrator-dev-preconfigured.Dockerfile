
# based on the official non-root user docu for vs-code container development
# https://code.visualstudio.com/docs/remote/containers-advanced#_adding-a-nonroot-user-to-your-dev-container

ARG FROM_IMAGE_REGISTRY=registry.gitlab.com
ARG FROM_IMAGE_TAG
ARG FROM_IMAGE_NAME
FROM $FROM_IMAGE_REGISTRY/$FROM_IMAGE_NAME:$FROM_IMAGE_TAG

USER root

######## install python packages ###########
ADD docker/preconfiguredServices/requirements-dev.txt .
RUN pip3 install -r requirements-dev.txt

RUN apt-get update && apt-get install -y sudo bash-completion nano

# add the user to sudoers
RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# reset the default user with enlarged config
USER ${USERNAME}
WORKDIR /workspace/dobas/

# repair git cli commands, waiting for non-closed editor
ENV EDITOR=nano

# enable bash-completion for current user, sourced by bashrc
RUN echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
