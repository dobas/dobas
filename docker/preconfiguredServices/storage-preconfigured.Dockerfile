FROM panubo/sshd:latest

RUN apk update && \
    apk add borgbackup && \
    rm -rf /var/cache/apk/*

COPY ./storageSSHHostKeys/ /etc/ssh/keys/
COPY ./sshUserKeys/id_ed25519.pub /etc/authorized_keys/dobas
RUN chmod 600 /etc/ssh/keys/*

RUN mkdir -p /storage
RUN chown 1000:1000 /storage/
