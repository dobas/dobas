import docker
import os
from unittest import TestCase
import subprocess


class TestContentProvider(TestCase):
    def setUp(self):
        self.docker_client = docker.from_env()
        # create example set of service volume combinations
        process = subprocess.Popen(args=['docker-compose', '-f', 'dobas/test/docker-compose.yml', 'up', '-d'],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        self.assertEqual(process.returncode, 0,
                         msg="process failed with stdout {} and stderr {}".format(stdout, stderr))

    def tearDown(self):
        process = subprocess.Popen(args=['docker-compose', '-f', 'dobas/test/docker-compose.yml', 'down', '--volumes'],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        self.assertEqual(process.returncode, 0,
                         msg="process failed with stdout {} and stderr {}".format(stdout, stderr))

    def test_startContainerWithExternalVolumesToBackup(self):
        # find volumes to backup
        volumes_to_backup = []
        for container in self.docker_client.containers.list(all=True):
            volumes_to_backup.extend(
                [mount for mount in container.attrs.get('Mounts')])

        remapped_volumes = {}
        # remap volume destinations
        for volume in volumes_to_backup:
            # handle named volumes
            if "Name" in volume:
                remapped_volumes[volume.get("Name")] = {
                    'bind': os.path.join('/', "backup", volume.get("Name")),
                    'mode': 'ro'
                }
            # handle non-named bind volumes
            else:
                remapped_volumes[volume.get("Source")] = {
                    'bind': os.path.join(os.sep, "backup", volume.get("Destination").replace(os.sep, "_")),
                    'mode': 'ro'
                }

        # volumes:
        # A dictionary to configure volumes mounted inside the container.
        # The key is either the host path or a volume name, and the value is a dictionary with the keys:
        #   - bind The path to mount the volume inside the container
        #   - mode Either rw to mount the volume read/write, or ro to mount it read-only.
        backup_folder_listing = self.docker_client.containers.run(
            image='alpine',
            command='bin/sh -c "ls -1 /backup/"',
            remove=True,
            volumes=remapped_volumes
        )

        # we expect 3 volumes to be mounted and get finishing \n in stdout, so we test against 4 expected volumes
        self.assertEqual(len(backup_folder_listing.split(b"\n")), 4,
                         msg="backup_folder_listing {}".format(backup_folder_listing))

        self.assertEqual(backup_folder_listing,
                         b'_tmp_src\ntest_named-volume\ntest_named-volume-bind\n')
