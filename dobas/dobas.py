
"""Borg backup
Usage:
  dobas.py backup [options]
  dobas.py restore [options]
  dobas.py (-h | --help)
  dobas.py --version

Options:
  -c <file>, --config <file>  Borg configuration file [default: x]
  -d, --dry      Make dry run. Don't change anything.
  --host <host>  Docker host [default: unix://var/run/docker.sock]
  -v, --verbose  Increase verbosity
  -h, --help     Show this screen.
  --version      Show version.

- find subjects to backup
- for each subject:
   do_subject specific backup


- subject types:
  - pure docker volume
"""
from dataclasses import dataclass
import logging
import sys
import docker
import subprocess
from pathlib import PurePath


@dataclass
class DobasCfg:
    dry_run: bool = False
    verbosity: int = logging.WARNING
    borg_prune_rules: str = "--keep-daily 14 --keep-monthly 10"


class DobasStrategy(list):
    @staticmethod
    def find_enabled_volumes() -> [docker.models.volumes.Volume]:
        """ get volumes to be handled

        list docker volumes labled with "orchestrator.enable: true"

        return list of volumes
        """
        client = docker.from_env()

        print(f"docker client: {client}")

        volumes = [volume for volume in client.volumes.list() if bool(
            volume.attrs.get('Labels').get('orchestrator.enable'))]

        return volumes

    @staticmethod
    def find_enabled_container_volumes() -> [docker.models.volumes.Volume]:
        return []

    @classmethod
    def discover(cls):
        s = DobasStrategy()
        for rule in [DobasStrategy.find_enabled_volumes, DobasStrategy.find_enabled_container_volumes]:
            s += rule()
        return s


class DobasRunner(object):
    def __init__(self, client: docker.DockerClient, cfg: DobasCfg = DobasCfg()):
        self.cfg = cfg
        self.client = client

    def run(self, strategy: DobasStrategy = None):
        for element in strategy:
            if type(element) is docker.models.volumes.Volume:
                self.volume_backup_handler(volume=element)

    def prune(self):
        ans = subprocess.run(["borg", "prune", "-v", "--list"] +
                             self.cfg.borg_prune_rules.split(" ") + ["::"])

        if ans.returncode == 0:
            pass
        else:
            print("oje")

    def volume_backup_handler(self, volume: docker.models.volumes.Volume = None):
        if volume:
            # risk of name duplicates here
            mounted_volume = {f"{volume.id}": {
                'bind': f"/mnt/{volume.id}", 'mode': 'ro'}}

            self.client.containers.run("alpine", command="ls -la /mnt",
                                       volumes=mounted_volume)

            ans = subprocess.run(["borg", "create", "--stats",
                                  "::{now}"] + volume.get('bind'))
            if ans.returncode > 0:
                print(f"NO BACKUP CREATED. BORG Collective failed to assimilate biological "
                      "entity. (Exitcode: {creation.returncode})", file=sys.stderr)
            else:
                print("borg exec done")

    def simple_folder_handler(self, paths: [PurePath] = None):
        if len(paths):
            pass


module_cfg = DobasCfg()


def do_backup():
    strategy = DobasStrategy.discover()
    runner = DobasRunner(client=docker.from_env(), cfg=module_cfg)
    runner.run(strategy)


def do_restore():
    print(__name__)


if __name__ == "__main__":
    do_backup()
