#!/bin/bash

# Preserve environment variables for cron tasks
jq -n env > borg_parameters.json

# prevent Permissions 0644 for '/home/<user>/.ssh/id_ed25519' are too open.
chmod 0600 ~/.ssh/*

# write fingerprint
mkdir -p ~/.ssh/
echo "$SSH_HOST_FINGERPRINT" >> ~/.ssh/known_hosts

echo "####################################################"
echo "IMPORTANT: Backup the encryption keys by backing"
echo "up your volume that has been mounted to /borgconfig!"
echo "####################################################"


if [ ! -f "/borgconfig/.config/borg/keys/backups" ]; then
    borg init --encryption=keyfile-blake2 "${BORG_REPO}"
fi
