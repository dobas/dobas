#!/bin/bash

"./init-backup.sh"

# install cronjobs
crontab -r &>/dev/null
echo "$CRON_INTERVAL_INCREMENTAL dobas backup >/var/log/output 2>/var/log/errors" | crontab -
echo "Installed incremental backup cronjob."

# run cron
sudo /usr/sbin/cron -f
