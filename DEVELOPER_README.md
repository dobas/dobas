# Developer Guide

## Service Stack Structure

1. **storage:** deamon also providing borg installation
2. **orchestrator:** derived production image with additional development features. This is the point we mount in and execture during developement
3. **dind:** clean docker deamon, encapsulated from your host deamon. The docker client in orchestrator is connected to dind. This is meant to give the opportunity to create and modify volumes to backup, but do not clutter your host docker deamon. Keep in mind, that only these volumes are seen from docker client inside the orchestrator.

``` mermaid
graph TD
    B1(base)
    B1 --> O2(orchestrator)
    B2 --> O3(orchestrator-preconfigured)
    B1 --> B2(preconfigured-base)
    D(docker:dind)
    B2 --> O1(orchestrator-dev-preconfigured)
    envusr[(ssh usr keys)] --> B2
    envusr --> S2  
    ssh(sshd) --> S2(storage-preconfigured)
    envhost[(ssh host keys)] --> S2
```

## Startup Environment Setup

This porject now follows the ideom to be developed as close as possible in the same environment, where the serivce is runned in production. Therefore the developer has two opportunities to use the same preconfigured docker based environment stack.

 1. **vscode** using remote development tools
 2. **plain** docker-compose based stack startup and development inside mount-point

Both ways uses the git-clone folder (with default name of dobas) and mounts it into the docker-stack. So working in this path and running git-commands in this directory is still the way to go.

### vscode using remote development tools

Visual studio code, here mentioned as vscode or vs-code is an open-source Microsoft developed IDE with large community support by a variaty of plugins. Keep in mind, the opensource builds, mostly provided in linux packages as vscode-oss are a pure build of the opensource version of vscode. This version does not contain the additional Microsoft APIs to run remote development plugins. So therefore ensure to **run the microsoft packaged binary of vscode, otherwise the remote development plugins do not work.** For more information, especially the plattform dependend IDE config, please visit the official documentation: <https://code.visualstudio.com/docs/remote/remote-overview>

### vscode: How to get started

1. install vs-code microsoft packages binary
2. go in left penal to plugins and activate the `Remote Development` plugin with detail name `ms-vscode-remote.vscode-remote-extensionpack`
3. hit F1 and and run `Remote-Containers: Rebuild and Reopen in Container`
4. optionally: hit the progress bar detail page, to see what is happining in background

If everything did work fine, you should get an vscode instance, where new commandlines instances of the IDE are started directly inside the orchestrator container, staying in `/workspace/dobas` as working directory.

### vscode: Information about configuration

The remote development capabilities for vs-code are configured inside the `.devcontainer/devcontainer.json`. This does refer to the docker-compose stack and describes which services is used to start the IDE's bash insances. Additionally some vscode plugins are downloaded and enabled, as demanded by the devcontainer.json. These plugins are aimed to help the developer developing with python, bash and git.

## plain: docker-compose based stack startup and development inside mount-point

If you don't want to stick to vscode or just want to work plain on cli, you can use the same preconfigured service stack, but without vscode.

### plain: How to get started

1. cd docker/preconfiguredServices
2. docker-compose up -d --build --remove-orphans
3. docker-compose exec orchestrator /bin/bash

### plain: Information about configuration

The `.env` holds the configuration, which docker-compose files are started up. The compose stack does trigger an in-place build for some services. Therefore the `--build` option is needed. The service stack is in configuration the same as consumed by vscode. Therefore some typicall configuration of this world is also present. For example the project does live in the `/workspace/dobas` folder, which is set to working-dir. So attaching to the bash of the service, you have full access to the mounted-project, but your are running inside the desired service environment.
