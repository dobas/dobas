# goals

## consistent backups

consistent backups are achieved by:

- docker-compose service stack
- docker service stack data (volumes)
- additionally we have to backup only, while the service in an consistent backup-mode

## state of the art backup mechanics

leveraging [Borg](https://borgbackup.readthedocs.io/en/stable/index.html#main-features) in background we provide common state of the art backup features:

- Space efficient storage (deduplication).
- Secure, authenticated encryption.
- Compression: LZ4, zlib, LZMA, zstd (since borg 1.1.4).
- Mountable backups with FUSE.
- Free and open-source software.
- Backed by a large and active open source community.
- Fast (incremental)
- Off-site backups

## easy to use

provide the traefik configuration way for docker backups:

- auto-discover services and volumes to backup
- good global config applying to most service stacks
- optional service spefific backup handling defined by service labels

# use cases

1. extend your existing docker service stack with a powerful and flexible backup feature
2. create backup of a specific docker service and it's data on demand
3. configurable backup storage (remote or local)
4. both: scheduled and manual triggered backup jobs
5. restore full docker service stack to storage state
6. desaster recovery: restore all service stacks to storage state
7. alert on backup failures
8. monitoring backup conistency via [borg check](https://borgbackup.readthedocs.io/en/stable/usage/check.html#borg-check)

# usage scenarios

## intended scenarios

1. backup docker-compose service stack
   - service stack is configured with docker-compose
   - compose files are located on the host running the docker deamon
   - compose service stack is executed with `docker-compose up -d`
   - the file path of all relevant configurations is not outside the workdir where `docker-compose up -d` was called

2. service stack without docker-compose
   - no serivce-stack bundle backup is expected, since it is not present

## not intended scenarios

- docker swarm mode
- docker-compose config files do not reside on the docker deamon host machine (e.g. runned via docker client connected to remote docker deamon)
- external config is passed to docker-compose cli

# modules

```mermaid
sequenceDiagram
    Host->>+dobas orchestrator: docker run
    loop  enabled services
      dobas orchestrator->>+Host: discovery
      Host->>-dobas orchestrator: subjects
      loop Every subjects
        dobas orchestrator->>+dobas worker: spawn
        dobas worker->>+dobas storage: do_backup
        dobas storage-->>-dobas worker: done
        dobas worker-->>-dobas orchestrator: done
      end
      dobas orchestrator-->>-Host: 42
    end
```

## dobas storage

The storage server, where the backups servers are storing their backups. It needs to be accessable via network from backup source server and its spawned worker.

## dobas orchestrator

The deamon which handles:

- runs on docker host-system next to the volumes and services to backup
- the docker service discovery
- the docker volume discovery
- the docker label configuration
- runs pre and post scripts on services to backup
- spawns backup worker in with given config

## dobas worker

The worker handles:

- is configured and spawned by backup source orchestrator
- accesses the volume to backup
- backups the volume to backup storage daemon
- specialized worker provides specific backup and restore functionality (e.g. database worker backups)

# feature-list

| feature name | scenarios | feature description |  milestone |
|-|-|-|-|-|
| traefik style backup source daemon configuration | 1,2 | Configuration map divided into global config and service (overridable) config. (Modes to implement: denylist, allowlist, backup storage, scheduling, pre and post scripts, ... )  | 1 |
| health report | 1,2 | reports if the backup on condfigured storage is valid. | 3 |
| health report error report: unauthenticated smtp | 1,2 | reports errors on problems to unauthenticated mailbox | 3 |
| volume restore | 1,2 | restores a specific volume to latest state or given date | 1 |
| service restore | 1,2 | restores service and its volmes to latest state or given date | 2 |
| desaster restore | 1,2 | restore all services and its volumes to latest or given date | 2 |
| plugin infrastructure: specialized backup and restore | 1,2 | provide interface to give specialized backup and restore funktionality for specific volumes. (E.g. database backups powered by database clients.) | 3 |
| host-system docker-compose service stack bundle and backup | 1 | bundle service config (docker-compose folder) with its volumes, allowing restore of service with its volumes. | 2 |
